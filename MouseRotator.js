if (Meteor.isClient) {
      
    var timer;
    var delay = 40;
    var currentX = 0;
    var currentY = 0;
    var oldX = 0;
    var oldY = 0;
    var vx = 20;
    var vxMin = -20;
    var vxMax = 20;
    var vy = 20;
    var vyMin = -20;
    var vyMax = 20;
    var frein = 0.9;
    var target;
    var inHand = false;
    var forceHand = .1;

    timer = Meteor.setInterval(function() {

        // mouse not dragged so
        if (!inHand) {
            // speed decrease
            vx = vx * frein; 
            vy = vy * frein; 
        }
        // range the speed value
        vx = Math.min(Math.max(vx, vxMin), vxMax); 
        vy = Math.min(Math.max(vy, vyMin), vyMax); 

        // change the current rotation
        currentX += vx;
        currentY += vy;

        rotate(currentX, currentY);

    }, delay);


    Meteor.startup(function() {
        // save target element
        target = $('.rotator');

        /* Active mouse events for hand controls */

        // out of hand mode
        $(window).mousedown(function(e) {
            inHand = true;

            // reset old position
            oldX = e.pageX;
            oldY = e.pageY;
            // and
            moveHand(e.pageX, e.pageY);

            return false;
        });
        // move hand mode
        $(window).mousemove(function(e) {
            if (inHand) moveHand(e.pageX, e.pageY);
            return false;
        });
        // out of hand mode
        $(window).mouseup(function(e) {
            inHand = false;
            return false;
        });
    });

    // control the velocity with mouse
    var moveHand = function(mouseX, mouseY) {
        // calcul of the mouse velocity
        var dx = mouseX - oldX;
        var dy = mouseY - oldY;
        // ajust velocity
        vx = dx * forceHand;
        vy = dy * forceHand;
    }



    var rotate = function(degX, degY) {
        target.css({'-webkit-transform' : 'rotate('+ degX +'deg)',
                     '-moz-transform' : 'rotate('+ degX +'deg)',
                     '-ms-transform' : 'rotate('+ degX +'deg)',
                     'transform' : 'rotate('+ degX +'deg)'
                 });
        // with 3d
        /*target.css({'-webkit-transform' : 'rotateX('+ degY +'deg)',
                     '-moz-transform' : 'rotateX('+ degY +'deg)',
                     '-ms-transform' : 'rotateX('+ degY +'deg)',
                     'transform' : 'rotateX('+ degY +'deg)'
                 });*/
    }

}
